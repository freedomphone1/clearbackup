/*
 * OAndBackupX: open-source apps backup and restore app.
 * Copyright (C) 2020  Antonios Hazim
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.clearos.clearsync.tasks

import android.net.Uri
import com.clearos.clearsync.activities.MainActivityX
import com.clearos.clearsync.handler.BackupRestoreHelper
import com.clearos.clearsync.handler.ShellHandler
import com.clearos.clearsync.items.ActionResult
import com.clearos.clearsync.items.AppInfo
import com.clearos.clearsync.items.BackupProperties

class RestoreActionTask(appInfo: AppInfo, oAndBackupX: MainActivityX, shellHandler: ShellHandler, restoreMode: Int,
                        private val backupProperties: BackupProperties, private val backupLocation: Uri)
    : BaseActionTask(appInfo, oAndBackupX, shellHandler, restoreMode, BackupRestoreHelper.ActionType.RESTORE) {

    override fun doInBackground(vararg params: Void?): ActionResult? {
        val mainActivityX = mainActivityXReference.get()
        if (mainActivityX == null || mainActivityX.isFinishing) {
            return ActionResult(app, backupProperties, "", false)
        }
        notificationId = System.currentTimeMillis().toInt()
        publishProgress()
        result = BackupRestoreHelper.restore(mainActivityX, shellHandler, app,
                mode, backupProperties, backupLocation)
        return result
    }
}